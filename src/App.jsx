import React, { Component } from "react";
import "./App.css";
import "./css/weather-icons.min.css";
import Search from "./components/Search/search";
import Weather from "./components/Weather/weather";
import NavBar from "./components/NavBar/navBar";
import weatherApi from "./adapters/weatherApi";

class App extends Component {
  constructor() {
    super();

    // get parsed search
    let savedCity = localStorage.getItem("city");

    if (savedCity) {
      this.handleSearch(savedCity);
      this.state = {
        activeComponents: []
      };
    } else {
      this.state = {
        activeComponents: [
          <Search key={0} searchHandler={city => this.handleSearch(city)} />
        ]
      };
    }
  }

  async handleSearch(cityName, units = "metric") {
    if (cityName === "") {
      alert("Please insert city name");
      return;
    }

    let weatherData = await weatherApi.getWeather(cityName, units);

    if (weatherData === "city not found") {
      alert("City not found, Please re-insert city name");
      return;
    }

    localStorage.setItem("city", cityName); // set search to local storage

    let forecastData = await weatherApi.getForecast(cityName, units); // 5 day forecast data

    this.setState({
      activeComponents: [
        <NavBar
          key={1}
          cityName={cityName}
          backHandler={() => this.handleBack()}
          unitsToggleHandler={e => this.handleToggleUnits(e)}
        />,
        <Weather key={2} weather={weatherData} forecast={forecastData} />
      ]
    });
  }

  handleBack() {
    console.log(" <-- Back ");

    localStorage.removeItem("city");

    this.setState({
      activeComponents: [
        <Search key={0} searchHandler={city => this.handleSearch(city)} />
      ]
    });
  }

  handleToggleUnits = async e => {
    const units = e.target.checked ? "imperial" : "metric";
    const key = e.target.checked ? 3 : 2;

    const cityName = localStorage.getItem("city");
    const weatherData = await weatherApi.getWeather(cityName, units);
    const forecastData = await weatherApi.getForecast(cityName, units);

    this.setState({
      activeComponents: [
        this.state.activeComponents[0],
        <Weather key={key} weather={weatherData} forecast={forecastData} />
      ]
    });
  };

  render() {
    return (
      <div className="App">
        <div className="header-box">{this.state.activeComponents}</div>
      </div>
    );
  }
}

export default App;
