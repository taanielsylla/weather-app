import React, { Component } from "react";
import "./search.css";
import logo from "./../../resources/icons/search.png";
import weatherApi from "./../../adapters/weatherApi";

class Search extends Component {
  state = {
    searchHandler: this.props.searchHandler,
    input: ""
  };

  onInputChange(e) {
    this.setState({ input: e.target.value });
  }

  async onLocationClick(e) {
    const location = await weatherApi.getLocation();
    this.state.searchHandler(location.city);
  }

  componentDidMount(){
    let input = document.getElementById("search-input");
    input.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
      if (event.keyCode === 13) {
       event.preventDefault();
        document.getElementsByClassName("searchBtn")[0].click();
      }
    });
  }

  render() {
    return (
      <div className="search-modal">
        <div className="search">
          <input id="search-input"
            type="text"
            placeholder="City"
            onChange={e => this.onInputChange(e)}
          />
          <button className="searchBtn">
            <img
              src={logo}
              alt=""
              onClick={() => this.state.searchHandler(this.state.input)}
            />
          </button>
        </div>

        <br />
        <p>or</p>

        <p>
          use my{" "}
          <a href="# " onClick={e => this.onLocationClick(e)}>
            current position
          </a>
        </p>
      </div>
    );
  }
}

export default Search;
