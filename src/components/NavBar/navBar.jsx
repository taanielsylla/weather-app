import React, { Component } from "react";
import "./navBar.css";
import backLogo from "./../../resources/icons/back.png";

class NavBar extends Component {
  state = {
    unitsToggleHandler: this.props.unitsToggleHandler,
    handleBack: this.props.backHandler,
    cityName: this.props.cityName
  };

  render() {
    return (
      <div className="navBar">
        <img src={backLogo} alt="" onClick={() => this.state.handleBack()} />
        <p>{this.state.cityName}</p>
        <label className="switch">
          <input
            type="checkbox"
            onChange={e => this.state.unitsToggleHandler(e)}
          />
          <span className="slider round" />
          <span className="label on">°c</span>
          <span className="label off">°f</span>
        </label>
      </div>
    );
  }
}

export default NavBar;
