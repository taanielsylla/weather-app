const iconMap = {
  "2d": "wi-day-thunderstorm",
  "2n": "wi-night-alt-thunderstorm",

  "3d": "wi-day-sleet",
  "3n": "wi-night-alt-sleet",

  "5d": "wi-day-rain",
  "5n": "wi-night-alt-rain",

  "6d": "wi-day-snow",
  "6n": "wi-night-alt-snow",

  "7d": "wi-windy",
  "7n": "wi-windy",

  "8d": "wi-day-sunny-overcast",
  "8n": "wi-night-alt-partly-cloudy",

  "clear d": "wi-day-sunny",
  "clear n": "wi-night-clear"
};

/**
 * Determine weather icon class
 *
 * @param {*} icon_id
 * @param {*} date
 */
export function getWeatherIconClass(icon_id, date) {
  const hours = date.getHours();
  const isDayTime = hours > 6 && hours < 20;

  const dayPeriod = isDayTime ? "d" : "n";

  let iconKey = Math.trunc(icon_id / 100);
  if (icon_id === 800) iconKey = "clear ";

  iconKey += dayPeriod;

  return "wi " + iconMap[iconKey];
}
