import React, { Component } from "react";
import "./weather.css";
import { getWeatherIconClass } from "./iconMap";

class weather extends Component {
  state = {
    handleBack: this.props.backHandler,
    weather: this.props.weather,
    forecast: this.props.forecast
  };

  render() {
    let {
      description,
      icon_id,
      temp,
      temp_max,
      temp_min,
      date_string,
      epochDate,
      units
    } = this.state.weather;

    return (
      <div className="weather-modal">
        <h1>{date_string}</h1>
        <h2>{description}</h2>
        <h3>
          {temp + units + " "}
          <i className={getWeatherIconClass(icon_id, epochDate)} />
          <ul className="weather">
            <li>high {temp_max + units}</li>
            <li>low {temp_min + units}</li>
          </ul>
        </h3>
        <ul className="forecast">
          {this.state.forecast.map((data, index) => {
            let { icon_id, temp, date_string, epochDate, units } = data;

            return (
              <li key={index}>
                <p>{date_string}</p>
                <i className={getWeatherIconClass(icon_id, epochDate)} />
                <p>{temp + units}</p>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default weather;
