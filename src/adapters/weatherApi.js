const API_KEY = "cf11b9a9ddb261028d7dd9dc4e00fa0e";

export default new class {
  /**
   * Make GET request to openweathermap api, return current weather data for given location.
   */
  async getWeather(cityName, units = "metric") {
    const url = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${API_KEY}&units=${units}`;

    const api_call = await fetch(url);
    const data = await api_call.json();

    if (data.message === "city not found") return "city not found";

    return {
      description: data.weather[0].description,
      icon_id: data.weather[0].id,
      temp: Math.trunc(data.main.temp),
      temp_max: Math.trunc(data.main.temp_max),
      temp_min: Math.trunc(data.main.temp_min),
      date_string: this.formatDate(data.dt),
      epochDate: new Date(data.dt * 1000),
      units: units === "metric" ? "°C" : "°F"
    };
  }

  /**
   * return weather forecast data for given location.
   */
  async getForecast(cityName, units = "metric") {
    const url = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=${API_KEY}&units=${units}`;

    const api_call = await fetch(url);
    const data = await api_call.json();

    if (data.message === "city not found") return "city not found";

    // return data for 5 day's
    let forecastData = data.list.filter((value, index, Arr) => {
      return index % 8 === 0;
    });

    let forecast = forecastData.map(data => {
      return {
        icon_id: data.weather[0].id,
        temp: Math.trunc(data.main.temp),
        date_string: this.formatDate(data.dt).split(",")[0], // day for forecast
        epochDate: new Date(data.dt * 1000),
        units: units === "metric" ? "°C" : "°F"
      };
    });

    return forecast;
  }

  async getLocation() {
    const url = "http://ipinfo.io/json?token=16b903bf8385c2";

    const api_call = await fetch(url);
    const data = await api_call.json();

    return data;
  }

  formatDate(epoch) {
    let d = new Date(epoch * 1000);
    var options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric"
    };

    return d.toLocaleDateString("en-US", options);
  }
}();
